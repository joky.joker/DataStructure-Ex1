#pragma once

template <class T>
class Node {
	Node * m_next = nullptr;
	T m_data;
public:
	Node(T data, Node * nextNode = nullptr) : m_data(data), m_next(nextNode) {};
	Node * getNextNode() { return m_next; }
	T getData() { return m_data; }
	Node<T> * deleteAfter();
	void insertAfter(Node * node);
	
};

template <class T>
inline Node<T>* Node<T>::deleteAfter()
{
	Node * nextNode = this->getNextNode();
	if (nextNode == nullptr) {
		return nullptr;
	}
	
	this->m_next = nextNode->getNextNode();
	return nextNode;
}

template<class T>
inline void Node<T>::insertAfter(Node * node)
{
	node->m_next = m_next;
	m_next = node;
}
