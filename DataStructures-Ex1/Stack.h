#pragma once
#include "Node.h"
#include <iostream>

template <class T>
class Stack {
	Node<T> * head = nullptr;
public:
	Stack<T>() = default;
	~Stack<T>();
	Stack(const Stack& stack) = delete;
	Stack(Stack&& stack) = delete;
	void MakeEmpty();
	bool IsEmpty() { return head == nullptr; }
	void Push(T item);
	T Pop();
};

template<class T>
inline void Stack<T>::MakeEmpty()
{
	Node<T> * temp = nullptr;
	while (head != nullptr) {
		temp = head;
		head = head->getNextNode();
		delete temp;
	}
}

template<class T>
inline void Stack<T>::Push(T item)
{
	head = new Node<T>(item, head);
}

template<class T>
inline T Stack<T>::Pop()
{
	if (IsEmpty()) {
		std::cerr << "ERROR: Stack underflow!" << std::endl;
		return T();
	}
	Node<T> * temp = head;
	T data = head->getData();
	head = head->getNextNode();
	delete temp;
	return data;
}

template<class T>
inline Stack<T>::~Stack()
{
	MakeEmpty();
}
