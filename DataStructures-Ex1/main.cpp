// Project:  test a recursive max function
// main.cpp : Main program.
//

/* instead of windows stuff, use standard header files:
// #include "stdafx.h"
*/
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
using namespace std;
// uncomment to disable assert()
// #define NDEBUG
#include <cassert>
#include <time.h>

const int MANUAL_INPUT = 10;

// myMax functions included here...
#include "myMax.h"

int main(int argc, char* argv[])
{
	int n;
	int *arr = NULL;
	const int NF = 4;  // Number of Functions to test
	int(*func[NF])(int *, int) = { myMax1, myMax2, myMax3 , myMax4};  // array of functions to test
	int fi;
	clock_t start, finish;
	double  duration;

	// Input or create an array
	cout << "Enter no. of elements:" << endl;
	cin >> n;
	assert((n > 0 && n <= 100000000));
	arr = new int[n];
	assert(arr != NULL);
	cout << "Enter elements..." << endl;
	if (n < MANUAL_INPUT) {
		for (int i = 0; i < n; i++)
			cin >> arr[i];
	} else {
		cout <<"Creating random array..." <<endl;
		srand((unsigned int)clock());
		for (int i = 0; i < n; i++)
			arr[i] = rand();
		cout << "Done" << endl;
	}
	
	// test myMax functions
	for (fi = 0; fi < NF; fi++) {
		// test the fi'th function
		start = clock();
		cout << "myMax" << (fi+1) << ": " << (*(func[fi]))(arr, n) << endl;
		finish = clock();
		duration = (double)(finish - start) / CLOCKS_PER_SEC;
		cout << "This took ";
		printf("%2.3f seconds\n", duration);
	}

	if (arr != NULL) {
		delete[] arr;
	}
	return 0;
}

