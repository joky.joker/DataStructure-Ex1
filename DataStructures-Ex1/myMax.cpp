#pragma once
#include <climits>
#include "myMax.h"
#include "Stack.h"



//Trivial solution, runs over the array.
int myMax1(int arr[], int size) {
	if (size <= 0) {
		return INT_MIN;
	}
	int max = arr[0];
	for (int i = 0; i < size; i++) {
		if (arr[i] > max) {
			max = arr[i];
		}
	}
	return max;
}

//Splits the array into 2 and recursivly computes max
int myMax2(int arr[], int size) {
	if (size <= 0) {
		return INT_MIN;
	}
	if (size == 1) {
		return arr[0];
	}
	int max1 = myMax2(arr, size - 1);
	int max2 = myMax2(arr + 1, size - 1);
	return MAX(max1, max2);
}

//like myMax2 but converted the recursion into use of stack
int myMax3(int arr[], int size) {
	if (size <= 0) {
		return INT_MIN;
	}
	Stack<ItemType> stack;
	ItemType curr;
	ItemType next;
	int returnValue = 0;
	curr = { arr, size, 0, 0, LINE::START};
	stack.Push(curr);
	while (!stack.IsEmpty()) {
		curr = stack.Pop();
		switch (curr.line)
		{
		case LINE::START:
			if (curr.size == 1) {
				returnValue = curr.arr[0];
				continue;
			}
			curr.line = LINE::AFTER_FIRST;
			stack.Push(curr);
			next = { curr.arr, curr.size - 1, 0, 0, LINE::START };
			stack.Push(next);
			break;
		case LINE::AFTER_FIRST:
			curr.max1 = returnValue;
			curr.line = LINE::AFTER_SECOND;
			stack.Push(curr);
			next = { curr.arr + 1, curr.size - 1, 0, 0,LINE::START };
			stack.Push(next);
			break;
		case LINE::AFTER_SECOND:
			returnValue = MAX(curr.max1, returnValue);
			break;
		}
	}

	return returnValue;
}

//Like myMax3 with addition optimization that saves a push and a pop each round.
int myMax4(int arr[], int size) {
	if (size <= 0) {
		return INT_MIN;
	}
	Stack<ItemType> stack;
	ItemType curr;
	bool popNextItem = false;
	int returnValue = 0;
	curr = { arr, size, 0, 0, LINE::START };
	do {
		if (popNextItem) {
			curr = stack.Pop();
		}
		switch (curr.line)
		{
		case LINE::START:
			if (curr.size == 1) {
				returnValue = curr.arr[0];
				popNextItem = true;
				continue;
			}
			curr.line = LINE::AFTER_FIRST;
			stack.Push(curr);
			curr = { curr.arr, curr.size - 1, 0, 0, LINE::START };
			popNextItem = false;
			break;
		case LINE::AFTER_FIRST:
			curr.max1 = returnValue;
			curr.line = LINE::AFTER_SECOND;
			stack.Push(curr);
			curr = { curr.arr + 1, curr.size - 1, 0, 0,LINE::START };
			popNextItem = false;
			break;
		case LINE::AFTER_SECOND:
			returnValue = MAX(curr.max1, returnValue);
			break;
		}
			
	} while (!stack.IsEmpty());

	return returnValue;
}