#pragma once


//Line enumerator used in ItemType.
enum class LINE {
	START,
	AFTER_FIRST,
	AFTER_SECOND,
};

//The item type used in myMax3 and myMax4
struct ItemType {
	int * arr;
	int size;
	int max1, max2;
	LINE line;
};


/*
	myMax1:
	input: array of integers and its size
	output: the max value in the array
	method:
		goes over every member of the array and compares it to its max.
		returns INT_MIN if size is below 1
*/
int myMax1(int arr[], int size);
/*
	myMax2:
	input: array of integers and its size
	output: the max value in the array
	method:
		takes 2 parts of the array, 0...size-1 and 1...size
		computes max of each one recursively then computes the max of the results.
		returns INT_MIN if size is below 1
*/
int myMax2(int arr[], int size);
/*
	myMax3:
	input: array of integers and its size
	output: the max value in the array
	method:
		same as myMax2, but in this case, the recursion is implemented using a Stack
		containing ItemTypes.
		returns INT_MIN if size is below 1
*/
int myMax3(int arr[], int size);
/*
	myMax4:
	input: array of integers and its size
	output: the max value in the array
	method:
		same as myMax2, but in this case, the recursion is implemented using a Stack
		containing ItemTypes. in addition there is an optimization that saves 1 push and 1 pop
		each time. Thus making this method almost twice faster then myMax3
	returns INT_MIN if size is below 1
*/
int myMax4(int arr[], int size);

//MAX macro, used for clairity.
#define MAX(a, b) (a) > (b) ? (a) : (b)